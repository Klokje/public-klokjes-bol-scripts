function OnLoad()
    PrintChat(" >> Anti Akali by Klokje")
end

function OnCreateObj(obj)
	if obj.name:lower():find("akali_smoke_bomb_tar_team_red") then 
		 DelayAction(function(obj)
		 	local slot = GetWardSlot()
		 	if slot == nil then return end 
		 	if GetDistance(obj) < 600 then 
		 		CastSpell(slot, obj.x, obj.z)
		 		return 
		 	end 
		 	local x,y,z = (Vector(obj) - Vector(myHero)):normalized():unpack()
	        local posX = myHero.x + (x * 600)
	        local posY = myHero.y + (y * 600)
	        local posZ = myHero.z + (z * 600)
	        tempLocation = Vector(posX, posY, posZ)
	         
	        if GetDistance(tempLocation,obj) < 850 then 
	        	CastSpell(slot, posX, posZ)
	        end

		 end, 0, {obj})
	end
end

function GetWardSlot()
	if GetInventorySlotItem(2043) and myHero:CanUseSpell(GetInventorySlotItem(2043)) == READY then
        return GetInventorySlotItem(2043)
    end
end 
