--[[
    Ping Blocker 0.1 by Klokje
    ================================================================================
    
    Info:
    Fully customize Ping Blocker. 


    Change log:
    0.1  
        -  Initial release 
       
]]
-- Code -----------------------------------------------------------------------------

function OnLoad()
    PrintChat(" >> Ping Blocker 0.1 by Klokje")  

    EmoteConfig = scriptConfig("Ping Blocker", "PingBlocker") 
    EmoteConfig:addParam("Block", "Block all", SCRIPT_PARAM_ONOFF, false)
    EmoteConfig:addSubMenu("Ally",tostring(myHero.team))
    EmoteConfig:addSubMenu("Enemy", tostring(TEAM_ENEMY))
	
    EmoteConfig[tostring(myHero.team)]:addParam("Block", "Block all allys", SCRIPT_PARAM_ONOFF, false)
    EmoteConfig[tostring(TEAM_ENEMY)]:addParam("Block", "Block all enemys", SCRIPT_PARAM_ONOFF, false)


    for i = 1, heroManager.iCount do
        local hero = heroManager:GetHero(i)
        local team = tostring(hero.team) 

        EmoteConfig[team]:addSubMenu(hero.charName, hero.charName)
        EmoteConfig[team][hero.charName]:addParam("Block", "Block All", SCRIPT_PARAM_ONOFF, false)
        EmoteConfig[team][hero.charName]:addParam("Alerts", "Block Alerts", SCRIPT_PARAM_ONOFF, false)
        EmoteConfig[team][hero.charName]:addParam("Danger", "Block Danger", SCRIPT_PARAM_ONOFF, false)
        EmoteConfig[team][hero.charName]:addParam("EnemyMissing", "Block EnemyMissing", SCRIPT_PARAM_ONOFF, false)
        EmoteConfig[team][hero.charName]:addParam("OnMyWay", "Block OnMyWay", SCRIPT_PARAM_ONOFF, false)
        EmoteConfig[team][hero.charName]:addParam("Retreat", "Block Retreat", SCRIPT_PARAM_ONOFF, false)
        EmoteConfig[team][hero.charName]:addParam("AssistMe", "Block AssistMe", SCRIPT_PARAM_ONOFF, false)
    end
end 

function OnPing(unit, ping)
	if EmoteConfig.Block then return false end 

	if EmoteConfig[tostring(unit.team)].Block then return false end 

	if EmoteConfig[tostring(unit.team)][unit.charName] then 
		if EmoteConfig[tostring(unit.team)][unit.charName].Block then return false end 
		if EmoteConfig[tostring(unit.team)][unit.charName].Alerts and ping.type == PING_ALERT then return false end 
		if EmoteConfig[tostring(unit.team)][unit.charName].Danger and ping.type == PING_DANGER then return false end 
		if EmoteConfig[tostring(unit.team)][unit.charName].EnemyMissing and ping.type == PING_ENEMY_MISSING then return false end 
		if EmoteConfig[tostring(unit.team)][unit.charName].OnMyWay and ping.type == PING_ON_MY_WAY then return false end 
		if EmoteConfig[tostring(unit.team)][unit.charName].Retreat and ping.type == PING_RETREAT then return false end 
		if EmoteConfig[tostring(unit.team)][unit.charName].AssistMe and ping.type == PING_ASSIST_ME then return false end 
	end 
end